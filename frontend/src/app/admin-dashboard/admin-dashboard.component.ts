import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  bsModalRef: BsModalRef;

  public usuarios = [];
  listUser: any;

  userDelete: any;

  constructor(
    private authService: AuthService,
    private router: Router,
    private modalService: BsModalService,
  ) { 
    this.getUsuarios()
  }

  ngOnInit() {

  }

  getUsuarios() {
    this.authService.getUsuarios().subscribe(
      res => {
        // for(let x; x < res.lenght; x=x+1){
        //   this.listUser = x
        //   console.log('hEYY', x)
        //   this.usuarios.push(this.listUser)
        // }
        this.usuarios = res.user
        console.log('Res', res.user)
        console.log('Res', this.usuarios)
      },
      err => console.log('Erro --->>', err)
    )
  }

  deleteUsers(user){
    this.userDelete = user
    console.log('Deletar usuario --->> ', this.userDelete)
    this.authService.deleteUsers(this.userDelete).subscribe(
      res => {
        console.log('Res', res)
      },
      err => console.log('Erro --->>', err)
    )
  }

  openModalWithComponent() {
    const initialState = {
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Modal with component'
    };
    this.bsModalRef = this.modalService.show(ModalContentComponent, {initialState});
    this.bsModalRef.content.closeBtnName = 'Close';
  }

}

@Component({
  selector: 'modal-content',
  template: `
    <div class="modal-header">
      <h4 class="modal-title pull-left">title</h4>
      <button type="button" class="close pull-right" aria-label="Close" (click)="bsModalRef.hide()">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <ul *ngIf="list.length">
        <li *ngFor="let item of list">item</li>
      </ul>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" (click)="bsModalRef.hide()">closeBtnName</button>
    </div>
  `
})

export class ModalContentComponent implements OnInit {
  title: string;
  closeBtnName: string;
  list: any[] = [];
 
  constructor(public bsModalRef: BsModalRef) {}
 
  ngOnInit() {
    this.list.push('PROFIT!!!');
  }
}
