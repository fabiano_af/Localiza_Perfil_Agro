import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private registerUrl = "http://localhost:3000/api/register";
  private loginUrl = "http://localhost:3000/api/login";
  private usuarios = "http://localhost:3000/api/usuarios";
  private deleteUsuarios = "http://localhost:3000/api/delete";

  constructor(
    private http: HttpClient,
    private router: Router,
  ) { }

  // /////////////////////////////////////////////////////////////////////
  // Buscar informações/dados
  // /////////////////////////////////////////////////////////////////////

  registerUser(user) {
    return this.http.post<any>(this.registerUrl, user)
  }

  loginUser(user) {
    return this.http.post<any>(this.loginUrl, user)
  }

  getUsuarios() {
    return this.http.get<any>(this.usuarios)
  }

  deleteUsers(user){
    console.log('Testando delete --->> ', user)
    return this.http.post<any>(this.deleteUsuarios, user)
  }

  // /////////////////////////////////////////////////////////////////////
  // Permissões
  // /////////////////////////////////////////////////////////////////////

  loggedIn() {
    return !!localStorage.getItem('token')
  }

  adminIn() {
    return !!localStorage.getItem('admin')
  }

  // /////////////////////////////////////////////////////////////////////
  // Local storage
  // /////////////////////////////////////////////////////////////////////

  logoutUser() {
    localStorage.removeItem('token')
    localStorage.removeItem('admin')
    this.router.navigate(['/login'])
  }

  getToken() {
    return localStorage.getItem('token')
  }

}
