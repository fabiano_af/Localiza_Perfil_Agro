import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginUserData = {}

  constructor(
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit() {

  }

  loginUser() {
    console.log('Login --->> ', this.loginUserData)
    this.authService.loginUser(this.loginUserData).subscribe(
      res => {
        console.log('Res', res)
        localStorage.setItem('token', res.token)
        if(res.admin == 'true') {
          localStorage.setItem('admin', 'true')
        }
        this.router.navigate(['/dashboard'])
      },
      err => console.log('Erro --->>', err)
    )
  }

}
