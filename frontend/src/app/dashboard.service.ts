import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  private dashboardUrl = "http://localhost:3000/api/dashboard";

  constructor(private http: HttpClient) { }

  getDashboard() {
    return this.http.get<any>(this.dashboardUrl)
  }

}
