const express = require('express')
const jwt = require('jsonwebtoken')
const router = express.Router()
const User = require('../models/user')
const mongoose = require('mongoose')
const db = "mongodb://botFacebook:bc123456!@dadosface-shard-00-00-aakyu.mongodb.net:27017,dadosface-shard-00-01-aakyu.mongodb.net:27017,dadosface-shard-00-02-aakyu.mongodb.net:27017/test?ssl=true&replicaSet=DadosFace-shard-0&authSource=admin&retryWrites=true"

mongoose.connect(db, err => {
    if(err) {
        console.log('Erro!' + err)
    }
    else {
        console.log('Estamos conectados galera')
    }
})

function verifyToken(req, res, next) {
    if(!req.headers.authorization) {
        return res.status(401).send('Request nao autorizado')
    }

    let token = req.headers.authorization.split(' ')[1]
    if(token === 'null') {
        return res.status(401).send('Request nao autorizado')
    }

    let payload = jwt.verify(token, 'secretKey')
    if(!payload) {
        return res.status(401).send('Request nao autorizado')
    }

    req.userId = payload.subject
    next()
}

router.get('/', (req, res) => {
    res.send('From API route')
})

router.post('/register', (req, res) => {
    let userData = req.body
    let user = new User(userData)
    user.save((error, registeredUser) => {
        if(error){
            console.log(error)
        }
        else {
            let payload = { subject: registeredUser._id }
            let token = jwt.sign(payload, 'secretKey')
            console.log('Usuario', registeredUser)
            res.status(200).send({token})
        }
    })
})

router.get('/usuarios', (req, res) => {
    User.find({}, (error, user) => {
        if(error) {
            console.log(error)
        }
        else {
            console.log('Usuarios', user.length)
            res.status(200).send({user})
        }
        
    })
})

router.post('/delete', (req, res) => {
    let id = req.body._id
    console.log('Delete', req.body)
    User.findByIdAndRemove({ _id: id }, (error, registeredUser) => {
        if(error){
            console.log(error)
        }
        else {
            console.log('Usuario deletado --->> ', registeredUser)
            res.status(200).send({registeredUser})
        }
    })
})
    
router.post('/login', (req, res) => {
    let userData = req.body

    User.findOne({ email: userData.email}, (error, user) => {
        if(error) {
            console.log(error)
        }
        else {
            if(!user) {
                console.log('Email invalido')
                res.status(401).send('Email invalido')
            }
            else if(user.password !== userData.password) {
                console.log('Senha invalida')
                res.status(401).send('Senha invalida')
            }
            else {
                let payload = { subject: user._id }
                let token = jwt.sign(payload, 'secretKey')
                console.log('User login', user)
                let admin = user.admin
                let id = user._id
                res.status(200).send({id, token, admin})
            }
        }
        
    })
})

router.get('/events', (req, res) => {
    let events = [
        {
            "_id": "1",
            "name": "Auto expo",
            "description": "Lorem ipsum",
            "date": "2012-04-23T18:25:43.511Z"
        }
    ]
    res.json(events)
})

router.get('/special', verifyToken, (req, res) => {
    let events = [
        {
            "_id": "2",
            "name": "Hey expo",
            "description": "Hey ipsum",
            "date": "2012-04-23T18:25:43.511Z"
        }
    ]
    res.json(events)
})

module.exports = router